# wordword

Because when things go closed source and for sale, everything goes to shit. 

Licensed under the cooperative Non-violent public license.

## wordlist
Alright, so I started this yesterday when an event triggered it, so I used
the sgb-list wordlist from the Stanford GraphBase found at 
https://www-cs-faculty.stanford.edu/~knuth/sgb-words.txt

I intend to go through and make the list about 6000 words long. 

*warning: I have not yet looked at every word in the list. There will likely be
some that I remove for content warning reasons*

## Running wordword
The only requirement is python be installed on the system. It can be invoked 
with:

* `python wordword1.py` for easy mode 
* `python wordword1.py --hard` for hard mode

## Playing wordword
In the CLI type your guess. Wordword will respond with a line like so: 

`| ^ | 1 | 1 | x | ^ | tests` 

(assuming your guessed word was tests)

In this line it means the 1st and 5th letters are correct, and there are 1 
instance each of letters 2 and 3. Letter 4 does not exist in the word. 

If your letter is in the correct place, it will not tell you how many there 
are. Also, if you guess a letter twice (for instance, you guess "tests" and
the word is "baste") you will get `| x | 1 | ^ | 2 | 1 |`. Note that both 
the "s" in the correct place, and the "s" in the incorrect place give a 
result. 

## Sharing results
I'm trying to do this *only* with standard python to keep install and play 
easy and bloat small. Because of this you will have to copy and paste, 
manually, the output at the end. It will look something like the following:

### Failure on Hard Mode
```
HM
| x | 1 | x | 1 | ^ |
| 1 | x | x | x | ^ |
| x | x | x | x | ^ |
| 1 | x | x | x | ^ |
| x | ^ | ^ | ^ | ^ |
x/5
```

### Success on Normal Mode
```
| x | x | x | 1 | x |
| 1 | 1 | x | x | x |
| ^ | 1 | 1 | x | ^ |
| ^ | ^ | ^ | ^ | x |
| ^ | ^ | ^ | ^ | ^ |
5/6
```

### New Feature: 
You can now use the --keep (to save), --past (to review), and --solution(to 
only see the answer) flags with the game. 

* --keep will save a copy of your last runthrough in wordword.dat
* --past will allow you to review your last play through and either share your
         results or guesses. 
* --solution will only print out the current solution
